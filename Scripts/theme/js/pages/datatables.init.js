$(document).ready(function () {
    $("table.display").DataTable();
    //$("#datatable").DataTable();

    var table = $("#datatable, #datatableMain, #datatablePopup, #datatableFeatured").DataTable({
        rowReorder: {
            selector: 'td:not(:first-child, :last-child)',
            paging: true
        },
        columnDefs: [
            { orderable: true, className: 'reorder', targets: '_all' },
            { orderable: true, targets: '_all' }
        ]
    });

    $('#datatable, #datatableMain, #datatablePopup, #datatableFeatured').on('row-reorder.dt', function (dragEvent, data, nodes) {
        var url = window.location.pathname.split("/");
        var controllerName = url[2];

        for (var i = 0, ien = data.length; i < ien; i++) {
            var rowData = table.row(data[i].node).data();

            //alert(rowData[1] +" | "+rowData[2] + " | " + data[i].oldPosition + " | " + data[i].newPosition)

            $.ajax({
                type: "GET",
                cache: false,
                contentType: "application/json; charset=utf-8",
                url: '/' + controllerName + '/UpdateRow',
                data: { Id: rowData[1], fromPosition: data[i].oldPosition, toPosition: data[i].newPosition },
                dataType: "json"
            });
        }
    });
});